import java.util.Random;

public class Deck{
	private Card[] cards;
	private int numberOfCards;
	private Random rng;
	
	//constructor
	public Deck() {
		this.numberOfCards = 52;
		this.cards = new Card[52];
		
		// array to initialize the deck
		String[] suits = new String[]{"Spades", "Clubs", "Hearts", "Diamonds"};
		for (int i = 0; i < cards.length; i++){
			int cardValue = (i+1) % 13;
			if (cardValue == 0){
				cardValue = 13;
			}
			
			//checks if each suit has had 13 cards then switches to the next one
			if (i > 38){
				cards[i] = new Card(suits[0], cardValue);
			}
			else if (i > 25){
				cards[i] = new Card(suits[1], cardValue);
			}
			else if (i > 12){
				cards[i] = new Card(suits[2], cardValue);
			}
			else
				cards[i] = new Card(suits[3], cardValue);
		}
	}
	
	//get method
	public Card[] getCards(){
		return this.cards;
	}
	
	//get Method for numberOfCards	
	public int length(){
		return this.numberOfCards;
	}
	
	public Card drawTopCard(){
		this.numberOfCards--;
		return this.cards[numberOfCards-1];
	}
	
	public String toString(){
		String output = "";
		for (int i = 0; i < this.numberOfCards; i++){
			output += this.cards[i].toString() + "\n";
		}
		return output;
	}
	
	public void shuffle(){
		for (int i= 0; i < this.numberOfCards-1;i++){
			Card temp = new Card("",0);
			int rng = new Random().nextInt(52);
			temp = this.cards[i];
			this.cards[i] = this.cards[rng];
			this.cards[rng] = temp;
		}
	}
}