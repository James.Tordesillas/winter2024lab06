public class LuckyCardGameApp{
	public static void main(String[] args){
		Card HJ = new Card("Heart", 11);
		System.out.println(HJ.toString());
		
		Deck solitaire = new Deck();
		System.out.println(solitaire.toString());
		
		solitaire.shuffle();
		System.out.println(solitaire.toString());
	}
}