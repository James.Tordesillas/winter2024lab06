public class Card{
	private String suit;
	private int value;
	
	// constructor
	public Card(String suit, int value){
		this.suit = suit;
		this.value = value;
	}
	
	//get Methods
	public String getSuit(){
		return this.suit;
	}
	public int getValue(){
		return this.value;
	}
	
	// toString Method
	public String toString(){
		if (this.value == 1){
			return "ACE of " + this.suit;
		}
		else if (this.value == 11){
			return "JACK of " + this.suit;
		}
		else if (this.value == 12){
			return "QUEEN of " + this.suit;
		}
		else if (this.value == 13){
			return "KING of " + this.suit;
		}
		else {
			return this.value + " of " + this.suit;
		}
	}
}